<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Corcel\Model\User;
use MikeMcLin\WpPassword\Facades\WpPassword;

class UsersController extends Controller
{
    public function user($id){
        $user = User::find($id);

        return view('user')
            ->withUser($user);
    }

    public function raw($id){
        $users = User::find($id);
        return view('raw')
            ->withUsers($users);
    }

    public function form(){
        return view('form');
    }

    public function gravar(Request $request){
        $user = new User();
        $user->user_login = $request->input('user_login');
        $user->user_pass = WpPassword::make($request->input('user_pass'));
        $user->user_nicename = $request->input('user_nicename');
        $user->user_email = $request->input('user_email');
        $user->display_name = $request->input('display_name');
        $user->save();
        $user->saveMeta('first_name', $request->input('first_name'), $user->ID);
        $user->saveMeta('last_name', $request->input('last_name'), $user->ID);
        $user->saveMeta('wp_user_level', 0, $user->ID);
        $user->save();
        return redirect()
            ->action('UsersController@form');
    }

    public function pass(){
        $text = '123';
        $pass = WpPassword::make($text);

        return view('pass')
            ->withPass($pass);

    }
}
